/* eslint-disable react/prop-types */
import React, {useEffect} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { singleCategory } from '../../actions/categoriesActions';
import TeacherQuizContainer from './TeacherQuizContainer';
import BackToDashboardButton from './../DashboardButton'

import { Button } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    boxShadow: 'none',
    paddingLeft: '20px',
    // maxWidth: 1000,
    minWidth: '60%',
  },
  title: {
    alignItems: 'center',
    paddingLeft: '20px',
  },
  tables: {
    paddingLeft: '20px',
    // minWidth: 500,
    // maxWidth: 1000,
    alignItems: 'center',
    boxShadow: 'none',
  },
}));

const TeacherCategoryPage = (props) => {
  const { id } = props.match.params;
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();

  const categoryContent = useSelector(state => state.categoryContent[id]);
  const isLoading = useSelector(state => state.categoryContent.isLoading);
  const userId = useSelector(state => state.auth.user.sub);

  const onLoad = (id) => {
    dispatch(singleCategory(id));
  };

  useEffect(() => { 
    onLoad(id);
  }, [id]);

  const content = categoryContent ?
    <>
      <Typography component="h4" variant="h4">
        {categoryContent[0].category}
      </Typography>
      <Typography component="h5" variant="h5">Quizzes</Typography>
      <TeacherQuizContainer data={categoryContent} />
    </> : null;

  const quizzesData = categoryContent ? 
    content :
    <>
      <Typography component="h5" variant="h5">No quizzes created for this category... yet! Add one now.</Typography>
      <Button variant="outlined" color="primary" onClick={() => history.push('/create-quiz')}>
        CREATE QUIZ
      </Button>
      
    </>

  const contentToDisplay = (categoryContent && !categoryContent[id] && isLoading) ? 
    <Typography component="h5" variant="h5">Category loading...</Typography> : quizzesData;

  return (
    <Paper className={classes.paper}>
      {contentToDisplay}
      <br />
      <BackToDashboardButton />
    </Paper>
  );
}

export default TeacherCategoryPage


