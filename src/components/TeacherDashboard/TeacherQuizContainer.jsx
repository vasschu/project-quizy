import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';

import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';

import StartQuizButton from '../SolveQuiz/StartQuizButton';
import TablePaginationActions from '../Search&TableWithPagination/TablePaginationActions';

const useStyles2 = makeStyles({
	table: {
		// width: 500,
	},
});

const TeacherQuizContainer = (props) => {
	const rows = props.data
	const classes = useStyles2();
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(5);
	const history = useHistory();

	const emptyRows = rows && (rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage));

	const handleChangePage = (event, newPage) => {
		setPage(newPage);
	};

	const handleChangeRowsPerPage = (event) => {
		setRowsPerPage(parseInt(event.target.value, 10));
		setPage(0);
	};

	return (
  <TableContainer component={Paper}>
    <Table className={classes.table} aria-label='custom pagination table'>
      <TableBody>
        {rows && (rowsPerPage > 0
						? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
						: rows
					).map((row) => (
  <TableRow key={row.id}>
    <TableCell component='th' scope='row'>
      {row.name}
    </TableCell>
    <TableCell style={{ width: 160 }} align='right'>
      <Button size="small" onClick={()=> history.push(`/users/quiz/${row.id}`)}>View</Button>
    </TableCell>
    <TableCell style={{ width: 160 }} align='right'>
      <StartQuizButton categoryId={row.category_id} quizId={row.id} />
    </TableCell>
  </TableRow>
					))}

        {emptyRows > 0 && (
          <TableRow style={{ height: 53 * emptyRows }}>
            <TableCell colSpan={6} />
          </TableRow>
					)}
      </TableBody>
      <TableFooter>
        <TableRow>
          <TablePagination
            rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
            colSpan={3}
            count={rows && rows.length}
            rowsPerPage={rowsPerPage}
            page={page}
            SelectProps={{
								inputProps: { 'aria-label': 'rows per page' },
								native: true,
            }}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
            ActionsComponent={TablePaginationActions}
          />
        </TableRow>
      </TableFooter>
    </Table>
  </TableContainer>
	);
}

export default TeacherQuizContainer
