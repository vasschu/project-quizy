import React from 'react';

import moment from 'moment'

import TableWithPagination from '../Search&TableWithPagination/TableWithPagination';

import { DataGrid } from '@material-ui/data-grid';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    boxShadow: 'none',
    minWidth: '90%'
  },
  table: {
    minWidth: 500,
  },
}));

const HistoryQuiz = (props) => {
  const {historyData, isLoading} = props;
  const classes = useStyles();

  const historyDataFormat = historyData !== null ? 
    historyData.map(el => {
      el.started_at = moment(el.started_at).format('MM/DD/YYYY, hh:mm');
      if(el.finished_at === null) {
        el.finished_at = 'Not submitted';
      } else if(el.finished_at !== 'Not submitted') {
        el.finished_at = moment(el.finished_at).format('MM/DD/YYYY, hh:mm');
      }

    return el
  }) : historyData;

  const table = (historyData === null || historyData.length === 0) ? 
    <Typography component="h5" variant="h5">No recorded history</Typography> :
    <Paper className={classes.paper}>
      <TableWithPagination 
        data={historyData} 
        isLoading={isLoading}
        firstColTitle='Username' 
        secondColTitle='Points' 
        thirdColTitle='First Name'
        fourthColTitle='Last Name'
        fifthColTitle='Finished at'
        firstCol="username" 
        secondCol="score" 
        thirdCol="first_name" 
        fourthCol="last_name"
        fifthCol="finished_at"
      />
    </Paper>
    // <DataGrid rows={historyData} columns={columns} pageSize={5} />

  const contentToDisplay = isLoading ? <Typography component="h5" variant="h5">History loading...</Typography> : table;

  return (
    <Paper className={classes.paper}>
      {contentToDisplay}
    </Paper>
  );
}

export default HistoryQuiz



