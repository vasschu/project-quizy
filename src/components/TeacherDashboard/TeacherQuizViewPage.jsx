import React, {useEffect} from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { quizHistory } from '../../actions/historyActions';

import HistoryQuiz from './HistoryQuiz';
import BackToDashboardButton from './../DashboardButton';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    boxShadow: 'none',
    minWidth: '100%'
  },
  table: {
    minWidth: 500,
  },
}));


const QuizViewPage = (props) => {
  const { quizId } = props.match.params;
  const classes = useStyles();

  const dispatch = useDispatch();
  const quizHistoryData = useSelector(state => state.quizHistory[quizId]);
  const isHistoryLoading = useSelector(state => state.quizHistory.isLoading);

  const historyFetch = (quizId) => dispatch(quizHistory(quizId));

  useEffect(() => {
    historyFetch(quizId)
  }, [quizId])

  const content = (quizHistoryData && quizHistoryData[0]) ? (
    <>
      <Typography component="h4" variant="h4">Quiz history</Typography>
      <HistoryQuiz historyData={quizHistoryData} isLoading={isHistoryLoading} />
    </>
  ) : <Typography component="h5" variant="h5">No history recorded</Typography>

  const contentToDisplay = (quizHistoryData && !quizHistoryData[quizId] && isHistoryLoading) ? 
    <Typography component="h5" variant="h5">History loading...</Typography> : content;

  return (
    <Paper className={classes.paper}>
      {contentToDisplay}
      <BackToDashboardButton />
    </Paper>
  );
}

export default QuizViewPage



