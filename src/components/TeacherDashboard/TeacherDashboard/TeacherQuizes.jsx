import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import Quiz from './Quiz.jsx'
import CreateQuizButton from './CreateQuizButton'
import { teacherQuizes } from '../../../actions/quizActions';

import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    margin: '25px',
    '& > *': {
      margin: theme.spacing(1),
    },
    minWidth: 200,
  },
  item: {
    maxWidth: '200px'
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));


const TeacherQuizes = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const teacherId = useSelector(state => state.auth.user.sub)

    useEffect(() => {
        dispatch(teacherQuizes(teacherId))
    }, [teacherId, dispatch])

    const availableCategories = useSelector(state => state.categories);
    const lastQuizes = useSelector(state => state.quiz.teacherQuizes);
    const isLoading = useSelector(state => state.quiz.isLoading);
    const reversedQuizes = JSON.parse(JSON.stringify(lastQuizes)).reverse();

    const listQuizes = reversedQuizes &&
    reversedQuizes.map((quiz, index) => (index < 5) ? 
      <Grid item key={quiz.id}>
        <Quiz className={classes.item} data={quiz} />
      </Grid> : null)

    return (
      <div className={classes.root}>
        <Typography variant="h4" component="h4">Your Latest Quizzes</Typography>
        <Grid container spacing={3}>
          {availableCategories ? 
            <CreateQuizButton /> : 
            <Typography>You need to create a category before creating a quiz</Typography>}
          {isLoading ? 
            <Typography>Loading quizzes...</Typography> :
            listQuizes}
        </Grid>
      </div>
    )
}

export default TeacherQuizes;
