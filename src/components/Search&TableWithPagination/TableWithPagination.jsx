import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import TablePagination from '@material-ui/core/TablePagination';

const useStyles = makeStyles((theme) => ({
  paper: {
    margin: theme.spacing(8, 4),
    // display: 'flex',
    // flexDirection: 'column',
    alignItems: 'center',
    boxShadow: 'none'
  },
  table: {
    minWidth: 500,
  },
}));

const TableWithPagination = ({data, isLoading, firstColTitle, secondColTitle, thirdColTitle, fourthColTitle, fifthColTitle,
     firstCol, secondCol, thirdCol, fourthCol, fifthCol}) => {
  const classes = useStyles();

  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return (
    // <Paper className={classes.paper}>
    <>
      {isLoading ? 
        <Typography>Still loading, hang in there...</Typography> :
        <>
          <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>{firstColTitle}</TableCell>
                  <TableCell align="right">{secondColTitle}</TableCell>
                  <TableCell align="right">{thirdColTitle}</TableCell>
                  <TableCell align="right">{fourthColTitle}</TableCell>
                  <TableCell align="right">{fifthColTitle}</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {data
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row) => (
                  <TableRow key={row[firstCol]}>
                    <TableCell component="th" scope="row">
                      {row[firstCol]}
                    </TableCell>
                    <TableCell align="right">{row[secondCol]}</TableCell>
                    <TableCell align="right">{row[thirdCol]}</TableCell>
                    <TableCell align="right">{row[fourthCol]}</TableCell>
                    <TableCell align="right">{row[fifthCol]}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={data.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
          />
        </>}
    </>
  );
}

export default TableWithPagination;
