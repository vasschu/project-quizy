import React from 'react';
import { useSelector } from 'react-redux';
import {useHistory} from 'react-router-dom'

import Button from '@material-ui/core/Button';

const BackToDashboardButton = () => {
  const role = useSelector(store => store.auth.user.role)
	const history = useHistory();


  return (
    <div>  
      <Button variant="outlined" color="primary" onClick={()=> history.push('/')}>Dashboard</Button>
    </div>
  );
}

export default BackToDashboardButton;
