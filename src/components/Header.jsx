import React, { useEffect } from 'react';
import { useHistory, NavLink } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';


import LogOut from './Auth/Logout'

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
      textDecoration: 'none',
      color: 'white',
      fontWeight: '500',
      fontSize: '25px'
    },
    dash: {
      flexGrow: 1,
      textDecoration: 'none',
      color: 'white',
      fontWeight: '400',
      fontSize: '20px',
      textAlign: 'right',
      paddingRight: '20px'
    },
  }));

const Header = () => {
  const history = useHistory();
  const classes = useStyles();
  const isAuth = useSelector(store => store.auth.isLogged);
  const role = useSelector(store => store.auth.user.role);
  useEffect(() => {
    if (!isAuth) {
        history.push('/login');
    }
}, [isAuth, history])

    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            <NavLink className={classes.title} to="/">Project Q</NavLink>
            {role === 'Teacher' &&
              <NavLink className={classes.dash} to="/teachers">
                <Button color='inherit'>Dashboard</Button>
              </NavLink>}
            {role === 'Student' &&
              <NavLink className={classes.dash} to="/students">
                <Button color='inherit'>Dashboard</Button>
              </NavLink>}
            {isAuth && <LogOut />}
          </Toolbar>
        </AppBar>
      </div>
    );
}

export default Header;
