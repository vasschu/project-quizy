import React, { useEffect } from 'react';
import { useHistory, NavLink } from 'react-router-dom';
import { useForm } from 'react-hook-form';

import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';

import { login } from '../../actions/аuthActions';
import { IMAGE } from '../../common/images';

import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Avatar from '@material-ui/core/Avatar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
    root: {
      height: '100vh',
    },
    image: {
      backgroundImage: `url(${IMAGE})`,
      backgroundRepeat: 'no-repeat',
      backgroundColor:
        theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
      backgroundSize: 'cover',
      backgroundPosition: 'center',
    },
    paper: {
      margin: theme.spacing(8, 4),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));

const LogIn = () => {
    const classes = useStyles();
    const { register, handleSubmit, errors} = useForm();  

    const dispatch = useDispatch();
    const history = useHistory();
    
    const onSubmit = data => dispatch(login(data));;
    const isLogged = useSelector(store => store.auth.isLogged);
    const isLoading = useSelector(store => store.auth.isLoading);

    useEffect(() => {
        if (isLogged) {
            history.push('/');
        }
    }, [isLogged,  history])

    return (
      <Grid container component="main" className={classes.root}>
        <CssBaseline />
        <Grid item xs={false} sm={4} md={7} className={classes.image} />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          <div className={classes.paper}>
            <Avatar className={classes.avatar} />
            <Typography component="h1" variant="h5">
              Login
            </Typography>
            {isLoading ? <Typography component="h1" variant="h5">Loging you in...</Typography> :
            <form className={classes.form} onSubmit={handleSubmit(onSubmit)} noValidate>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="username"
                label="Username"
                autoComplete="username"
                autoFocus
                name="username" 
                inputRef={register({required: true, minLength: 2})}
              />
              {errors.username && errors.username.type === 'required' 
                  && <p>Username is required</p>}
              {errors.username && errors.username.type === 'minLength' 
                  && <p>Username should be at least 2 symbols</p>}
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                inputRef={register({required: true, minLength: 2})}
              />
              {errors.password && errors.password.type === 'required' 
                    && <p>Password is required</p>}
              {errors.password && errors.password.type === 'minLength' 
                    && <p>Password should be at least 2 symbols</p>}
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
              >
                Login
              </Button>
              <Grid container>
                <Grid item xs>
                  <NavLink to='/register'>Don't have an account yet?</NavLink>
                </Grid>
              </Grid>
            </form>}
          </div>
        </Grid>
      </Grid>
  );
}

export default LogIn;
