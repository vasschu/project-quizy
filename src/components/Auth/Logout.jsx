import React from 'react';
import { useDispatch } from 'react-redux';

import { logout } from '../../actions/аuthActions';

import Button from '@material-ui/core/Button';

const LogOut = () => {
    const dispatch = useDispatch();
    return (
      <Button color='inherit' onClick={() => dispatch(logout())}>LogOut</Button>
    )
}

export default LogOut;
