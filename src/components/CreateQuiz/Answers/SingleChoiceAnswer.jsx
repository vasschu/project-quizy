import React, { useEffect } from 'react';

import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Radio from '@material-ui/core/Radio';

const SingleChoiceAnswer = ({ register, setValue, index, questionIndex, value, handleChange }) => {

    const isCorrect = +value === +index;

    useEffect(() => {
        setValue(`questions[${questionIndex}].answers[${index}].is_correct`, isCorrect ? 1 : 0)
    }, [setValue, questionIndex, index, value, isCorrect])
    

    return (
      <Grid container spacing={3}>
        <Grid item>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            className="Answer"
            label="Answer"
            autoComplete="Answer"
            autoFocus
            style={{width: 300}}
            name={`questions[${questionIndex}].answers[${index}].answer_text`}
            inputRef={register({ required: true })}
          />
        </Grid>
        <Grid item>
          <Radio
            checked={isCorrect}
            onChange={handleChange}
            value={index}
            name={`questions[${questionIndex}].answers[${index}].is_correct`}
            inputProps={{ 'aria-label': 'A' }}
            inputRef={register({ name: `questions[${questionIndex}].answers[${index}].is_correct`, 
                            value: isCorrect ? 1 : 0})}
          />
        </Grid> 
      </Grid>
    )
}

export default SingleChoiceAnswer;
