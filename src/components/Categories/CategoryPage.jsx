import React, {useEffect} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { Button } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

import { singleCategory } from '../../actions/categoriesActions';
import { history } from '../../actions/historyActions';
import QuizContainer from '../StudentDashboard/QuizContainer';
import History from '../StudentDashboard/StudentDashboard/History';
import BackToDashboardButton from './../DashboardButton'

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    boxShadow: 'none',
    // maxWidth: 1000,
    minWidth: '80%',

  },
  table: {
    minWidth: 500,
  },
}));

const CategoryPage = (props) => {
const { id } = props.match.params;

const classes = useStyles();
const dispatch = useDispatch();
const historyBrowser = useHistory();

const categoryContent = useSelector(state => state.categoryContent[id])
const isLoading = useSelector(state => state.categoryContent.isLoading)
const historyData = useSelector(state => state.history.history);
const isHistoryLoading = useSelector(state => state.history.isLoading);
const userId = useSelector(state => state.auth.user.sub);

const onLoad = (id) => {
  dispatch(singleCategory(id));
  dispatch(history(userId));
};

useEffect(() => onLoad(id), [id])

const solvedQuizIds = (historyData && historyData[0]) ? 
  historyData.map(el => el.quiz_id) : [];

const historyForCategory = (historyData && historyData[0]) ? 
  historyData.filter(el => +el.category_id === +id) : [];

const unsolvedQuizzes = (categoryContent && solvedQuizIds) ? 
  categoryContent.filter(el => !solvedQuizIds.includes(el.id)) : 
  categoryContent;

const isContentAvailable = categoryContent ? true : false;

const solvedAndUnsolvedQuizes = isContentAvailable ?
  <>
    <Typography component="h4" variant="h4">
      {categoryContent[0].category}
    </Typography>
    {unsolvedQuizzes[0] ? (
      <Paper className={classes.paper}>
        <Typography component="h5" variant="h5">Unsolved Quizzes</Typography>
        <QuizContainer data={unsolvedQuizzes} />
      </Paper>) : <p> WOW! You have solved all quizzes. Good job!</p>}

    {historyForCategory[0] ? (
      <Paper className={classes.paper}>
        <Typography component="h5" variant="h5">Solved Quizzes</Typography>
        <History historyData={historyForCategory} isLoading={isHistoryLoading} />
      </Paper>
    ) : <p> No solved quizzes recorded</p>}
  </> : null;

const quizzesData = isContentAvailable ? 
  solvedAndUnsolvedQuizes :
  <>
    <Typography component="h5" variant="h5">No quizzes created for this category... yet! Come back later.</Typography>
    <Button onClick={() => historyBrowser.goBack()}>
      Go back
    </Button>
  </>

const contentToDisplay = (isContentAvailable && isLoading) ? 
  <Typography component="h5" variant="h5">Category loading...</Typography> : quizzesData;

  return (
    
    <Paper className={classes.paper}>
      {contentToDisplay}
    </Paper>
  );
};

export default CategoryPage;


