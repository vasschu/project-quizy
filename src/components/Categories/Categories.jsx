import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import Category from './Category'
import AddCategory from './AddCategory'
import { categories } from '../../actions/categoriesActions';

import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    margin: '25px',
    '& > *': {
      margin: theme.spacing(1),
    },
    minWidth: 200,
  },
  item: {
    maxWidth: '200px'
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));


const Categories = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const onLoad = () => dispatch(categories());

    useEffect(() => {
    onLoad()
    }, [])

    const availableCategories = useSelector(state => state.categories);
    const role = useSelector(state => state.auth.user.role)  

    return (
      <div className={classes.root}>
        <Typography variant="h4" component="h4">Categories</Typography>
        <Grid container spacing={3}>
          {role === 'Teacher' && <AddCategory />}
          {availableCategories.categories &&
      availableCategories.categories.map(category => 
        <Grid item key={category.id}>
          <Category className={classes.item} data={category} />
        </Grid>)}
        </Grid>
      </div>
    )
}

export default Categories;
