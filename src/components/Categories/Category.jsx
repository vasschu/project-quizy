/* eslint-disable react/prop-types */
import React from 'react';
import { useHistory } from 'react-router-dom';

import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';


const useStyles = makeStyles({
  root: {
    minWidth: 200,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

export default function Category(props) {
  const classes = useStyles();
  const { id, category } = props.data;
  const history = useHistory();
 
  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography variant="h6" component="h4">
          {category}
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small" onClick={()=> history.push(`/categories/${id}`)}>Browse Quizzes</Button>
      </CardActions>
    </Card>
  );
}



