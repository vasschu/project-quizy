import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { history } from '../../actions/historyActions';

import TableWithPagination from '../Search&TableWithPagination/TableWithPagination'
import SearchForm from '../Search&TableWithPagination/SearchForm'
import BackToDashboardButton from './../DashboardButton'

import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    boxShadow: 'none',
  },
  table: {
    minWidth: 500,
  },
}));

const UserHistoryPage = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  
  const historyData = useSelector(state => state.history.history);
  const userInfo = useSelector(state => state.auth.user);
  const isHistoryLoading = useSelector(state => state.history.isLoading);

  const [ historyDataView, setHistoryDataVIew ] = useState(historyData);

  const userId = userInfo.sub;
  useEffect(() => {
    dispatch(history(userId))
  }, [dispatch, userId])


  const handleSearch = (search) => {
    const searchedQuiz = historyData.filter(quiz => quiz.name.toLowerCase().includes(search.toLowerCase()));
    setHistoryDataVIew(searchedQuiz)
  }


  return (
    <Paper className={classes.paper}>
      <Typography component="h4" variant="h4">Solved Quizzes History</Typography>
      <SearchForm handleSearch={handleSearch} searchTitle='Search a quiz' />
      <TableWithPagination 
        data={historyDataView} 
        isLoading={isHistoryLoading}
        firstColTitle='Quiz Name' 
        secondColTitle='Points' 
        thirdColTitle='Started At'
        fourthColTitle='Finished At'
        firstCol='name' 
        secondCol='score' 
        thirdCol='started_at' 
        fourthCol='finished_at' 
      />
      <br />
      <br />
      <BackToDashboardButton />

    </Paper>
  );
}

export default UserHistoryPage



