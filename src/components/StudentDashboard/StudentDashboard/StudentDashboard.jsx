/* eslint-disable react/prop-types */
import React, {useEffect} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import Leaderboard from './Leaderboard';
import History from './History';
import Categories from '../../Categories/Categories'

import { leaderboard } from '../../../actions/leaderboardActions';
import { history } from '../../../actions/historyActions';

import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';



const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    boxShadow: 'none',
    paddingLeft: '20px',
    // maxWidth: 1000,
    // minWidth: 500,
  },
  title: {
    alignItems: 'center',
    paddingLeft: '20px',
  },
  tables: {
    paddingLeft: '20px',
    // minWidth: 500,
    // maxWidth: 1000,
    alignItems: 'center',
    boxShadow: 'none',
  },
}));

const StudentDashboard = () => {
const dispatch = useDispatch();
const classes = useStyles();
const leaderboardFetch = (id = null) => dispatch(leaderboard(id));
const historyFetch = (id) => dispatch(history(id));
const browserHistory = useHistory();



const historyData = useSelector(state => state.history.history);
const userId = useSelector(state => state.auth.user.sub);
const isHistoryLoading = useSelector(state => state.history.isLoading);
const leaderboardData = useSelector(state => state.leaderboard.leaderboard);

const leaderboardRows = (leaderboardData && leaderboardData.length > 10) ? 
  leaderboardData.slice(0, 10) : leaderboardData

const historyBoardData = (historyData !== null && historyData.length > 5) ? 
  historyData.slice(0,5) : historyData

useEffect(() => {
  leaderboardFetch()
  historyFetch(userId)
}, [])

	return (
  <div className={classes.root}>
    <div className='categories-contianer'>
      <Categories />
    </div>
    <div className={classes.tables}>
      <Typography className={classes.title} component="h4" variant="h4">Leaderboard</Typography>
      <Leaderboard data={leaderboardRows} />
      <Button size="large" color="primary" onClick={()=> browserHistory.push('/leaderboard')}>Full Leaderboard</Button>
    </div>
    <br />
    <div className={classes.tables}>
      <Typography className={classes.title} component="h4" variant="h4">Last Solved Quizzes</Typography>
      <History historyData={historyBoardData} isLoading={isHistoryLoading} />
      <Button size="large" color="primary" onClick={()=> browserHistory.push(`/users/${userId}`)}>Full History</Button>
    </div>
  </div>
	);
};

export default StudentDashboard;
