import React from 'react';

import TableWithPagination from './../../Search&TableWithPagination/TableWithPagination'

import moment from 'moment'
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    boxShadow: 'none',

  },
  table: {
    minWidth: 500,
  },
}));

const Leaderboard = ({ data }) => {
  const classes = useStyles();

  const table = data ? 
  (
    <Paper className={classes.paper}>
      <TableWithPagination 
        data={data} 
        isLoading={!data}
        firstColTitle='First Name' 
        secondColTitle='Last Name' 
        thirdColTitle='Total Score'
        fourthColTitle='Username'
        firstCol='first_name' 
        secondCol='last_name' 
        thirdCol='sum' 
        fourthCol='username'
      />
    </Paper>
  ) :
    <h3>leaderboard loading...</h3>

  return (
    <div style={{ height: 400, width: '100%' }}>
      {table}
    </div>
  );
}

export default Leaderboard;




