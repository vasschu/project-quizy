import {
	LOGIN_SUCCESS,
	REGISTER_SUCCESS,
	LOGIN_FAIL,
	LOGOUT_SUCCESS,
	REGISTER_FAIL,
	USER_LOADING,
	USER_LOADED,
	AUTH_ERROR,
} from '../actions/types';

import decode from 'jwt-decode';

const initialState = {
	token: localStorage.getItem('token'),
	isLogged: !!localStorage.getItem('token') &&
		new Date(JSON.parse(localStorage.getItem('userData')).exp * 1000) >
			new Date(),
	isLoading: false,
	user: JSON.parse(localStorage.getItem('userData')) || {},
};

const loggedReducer = (state = initialState, action) => {
	switch (action.type) {
		case USER_LOADING:
			return {
				...state,
				isLoading: true,
			};
		case USER_LOADED:
			localStorage.removeItem('activeQuiz');
			return {
				...state,
				isLoading: false,
			};
		case LOGIN_SUCCESS:
		case REGISTER_SUCCESS:
            localStorage.clear();
			localStorage.setItem('token', action.payload.token);
			const userDetails = decode(action.payload.token);
			localStorage.setItem('userData', JSON.stringify(userDetails));
			return {
				...state,
				token: action.payload.token,
				isLogged: true,
				user: userDetails,
			};
		case LOGIN_FAIL:
		case LOGOUT_SUCCESS:
		case REGISTER_FAIL:
		case AUTH_ERROR:
			localStorage.clear();
			return {
				...state,
				token: null,
				isLogged: false,
				user: {},
			};
		default:
			return state;
	}
};

export default loggedReducer;
