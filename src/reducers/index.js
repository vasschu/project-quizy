import { combineReducers } from 'redux';

import auth from './auth';
import categoriesReducer from './categories';
import leaderboardReducer from './leaderboard';
import historyReducer from './history';
import categorySingleReducer from './categorySingle';
import quizReducer from './quiz';
import solveQuizReducer from './solveQuiz';
import quizHistoryReducer from './quizHistory';

const allReducers = combineReducers({
	auth: auth,
	categories: categoriesReducer,
	leaderboard: leaderboardReducer,
	history: historyReducer,
	categoryContent: categorySingleReducer,
	quiz: quizReducer,
	activeQuiz: solveQuizReducer,
	quizHistory: quizHistoryReducer,
});

const rootReducer = (state, action) => {   
	// Clear all data in redux store to initial.
	if(action.type === 'LOGOUT_SUCCESS')
	   state = undefined;
	
	return allReducers(state, action);
 };
 export default rootReducer;
