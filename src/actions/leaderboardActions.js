import {
	STUDENTS_LEADERBOARD_LOAD,
	STUDENTS_LEADERBOARD_FAIL,
} from './types';

import axios from '../common/http-common';
import { handleError } from '../common/handleErrors';

export const leaderboard = (id) => (dispatch) => {
	axios
		.get(`/users/leaderboard/${id}`)
		.then((res) => {
			const usersSortedByScore = res.data.result.sort((a, b) => b.sum - a.sum);
			dispatch({
				type: STUDENTS_LEADERBOARD_LOAD,
				payload: usersSortedByScore,
			});
		})
		.catch((err) => {
			dispatch({
				type: STUDENTS_LEADERBOARD_FAIL,
			});
			// handleError(err);
		});
};
